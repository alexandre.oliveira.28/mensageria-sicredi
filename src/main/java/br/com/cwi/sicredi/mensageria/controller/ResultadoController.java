package br.com.cwi.sicredi.mensageria.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.cwi.sicredi.mensageria.constantes.Constantes;
import br.com.cwi.sicredi.mensageria.dto.ResultadoDTO;
import br.com.cwi.sicredi.mensageria.service.RabbitMQService;

@RestController
@RequestMapping(value = "/resultado")
public class ResultadoController {
	
	@Autowired
	private RabbitMQService rabbitMQService;

	@GetMapping
	private ResponseEntity getResultado(@RequestBody ResultadoDTO resultadoDto) {
		this.rabbitMQService.enviaMensagem(Constantes.FILA_RESULTADO_VOTACAO, resultadoDto);
		return new ResponseEntity<>(HttpStatus.OK);
	}
}